-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.26-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for testdb
CREATE DATABASE IF NOT EXISTS `testdb` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `testdb`;

-- Dumping structure for table testdb.tabel_caini
CREATE TABLE IF NOT EXISTS `tabel_caini` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(20) NOT NULL,
  `userBreed` varchar(50) NOT NULL,
  `userSex` enum('F','M') DEFAULT NULL,
  `userAge` varchar(15) NOT NULL,
  `userLocatie` varchar(20) NOT NULL,
  `userPic` blob NOT NULL,
  `userTel` varchar(16) NOT NULL,
  `userAcc` int(11) NOT NULL,
  `adoptat` tinyint(3) NOT NULL DEFAULT '1',
  PRIMARY KEY (`userID`)
) ENGINE=MyISAM AUTO_INCREMENT=228 DEFAULT CHARSET=latin1;

-- Dumping data for table testdb.tabel_caini: 19 rows
/*!40000 ALTER TABLE `tabel_caini` DISABLE KEYS */;
INSERT INTO `tabel_caini` (`userID`, `userName`, `userBreed`, `userSex`, `userAge`, `userLocatie`, `userPic`, `userTel`, `userAcc`, `adoptat`) VALUES
	(173, 'Atena', 'Husky', 'M', '11', 'Oradea', _binary 0x3330393131392E6A7067, '0745141242', 2, 1),
	(172, 'Bob', 'Pitbull', 'M', '2', 'Maramures/Baia Mare', _binary 0x3534373036362E6A7067, '0741231232', 2, 1),
	(195, 'Rocky', 'Maidanez', 'M', '3', 'Cluj', _binary 0x3431363930392E6A7067, '0742352423', 30, 1),
	(196, 'Odin', 'Pitbull', 'F', '6', 'Maramures', _binary 0x3232313936312E6A7067, '087523523', 30, 1),
	(190, 'Gowy', 'Golden Retriever', 'F', '1 luna', 'Constanta', _binary 0x3837363135322E6A7067, '0745253135', 2, 1),
	(189, 'Pimp', 'chiwawa', 'M', '2', 'Baia Sprie', _binary 0x3336383633372E6A7067, '0731242142', 2, 0),
	(174, 'Tret', 'Husky', 'M', '5', 'Maramures', _binary 0x3931313536382E6A7067, '0754251241', 29, 1),
	(198, 'Pablo', 'Maidanez', 'F', '3', 'Maramures', _binary 0x3837313832312E6A7067, '0742342423', 30, 1),
	(220, 'Cora', 'Maidanez', 'F', '2 ani', 'Baia Mare           ', _binary 0x3938333434352E6A7067, '03252324324', 2, 1),
	(217, 'Bob', 'Beagle', 'M', '2 luni', 'Baia Mare', _binary 0x39393035392E6A7067, '0123123267', 2, 1),
	(219, 'Laila', 'Maidanez', 'F', '2 ani si 9 luni', 'Baia Mare', _binary 0x3235383734392E6A7067, '07324234324', 2, 1),
	(218, 'Pufi', 'Maidanez', 'F', '2 ani', 'Baia Sprie', _binary 0x3730383437332E6A7067, '01665123267', 2, 0),
	(221, 'Petro', 'Maidanez', 'M', '1 luna', 'Constanta', _binary 0x3330393839302E6A7067, '0752343255', 2, 0),
	(222, 'Pufi', 'Maidanez', 'M', '1 luna', 'Constanta', _binary 0x3330373738392E6A7067, '0735235463', 11, 1),
	(223, 'Linda', 'Maidanez', 'F', '2 luni', 'Maramures', _binary 0x3434333035342E6A7067, '0743253426', 11, 1),
	(224, 'Athos', 'Maidanez', 'M', '5 ani', 'Cluj', _binary 0x3134363339322E6A7067, '0325231532', 2, 1),
	(225, 'Noro', 'Maidanez', 'M', '2 ani', 'Consanta', _binary 0x3332323630322E6A7067, '05633635727', 2, 1),
	(226, 'Kane', 'Dog German', 'M', '5 ani', 'Maramures', _binary 0x3832313335312E6A7067, '07326346234', 2, 1),
	(227, 'Nero', 'Maidanez', 'M', '3 luni', 'Zalau', _binary 0x35303932302E6A7067, '0843634732', 2, 1);
/*!40000 ALTER TABLE `tabel_caini` ENABLE KEYS */;

-- Dumping structure for table testdb.tabel_utilizatori
CREATE TABLE IF NOT EXISTS `tabel_utilizatori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

-- Dumping data for table testdb.tabel_utilizatori: ~6 rows (approximately)
/*!40000 ALTER TABLE `tabel_utilizatori` DISABLE KEYS */;
INSERT INTO `tabel_utilizatori` (`id`, `username`, `email`, `password`) VALUES
	(1, 'ioio', 'henter.ionut.1996@gmail.com', '17324235011af66d659cbb7a2d2cbe6e'),
	(2, 'ionut', 'henter.ionut.1996@gmail.com', '202cb962ac59075b964b07152d234b70'),
	(3, 'ionut2', 'ionut@gmail.com', '202cb962ac59075b964b07152d234b70'),
	(4, 'ionut3', 'ionut@gmail.com', '202cb962ac59075b964b07152d234b70'),
	(11, 'test100', 'test100@gmail.com', '202cb962ac59075b964b07152d234b70'),
	(29, 'Henter', 'henter.ionut.1996@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055'),
	(30, 'henter2', 'henter@gmail.com', '202cb962ac59075b964b07152d234b70'),
	(31, 'testfinal', 'testfinal@gmail.com', '202cb962ac59075b964b07152d234b70'),
	(32, 'adam', 'adam@gmail.com', '1d7c2923c1684726dc23d2901c4d8157');
/*!40000 ALTER TABLE `tabel_utilizatori` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
