<?php
	require_once 'dbconfig.php';
?>
<!DOCTYPE html PUBLIC>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" />
	<title>Licenta</title>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="bootstrap/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="css/app.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>  

<script> 
		$(document).ready(()=>{ 
			let numarAnunturi = 9;
			$("#loadButton").click(function(){
				numarAnunturi += 6;
				$("#anunturile").load("anunturi.php",{
					numarAnunturiNoi: numarAnunturi
					}); 
			});
		});
</script>

</head>

<body style="background: url(images/bg1.jpg) no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;">

<div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container-fluid">

        <div class="navbar-header">
        	<?php session_start();?> 
			<a class="navbar-brand" href="index.php" title='Acasa'>Acasa</a>
            <a class="navbar-brand" href="indexlogin.php">Adauga anunt</a>
           </div>
           <a class="navbar-brand nav navbar-nav navbar-right" href="indexlogin.php?logout='1'" style="color: red;"><?php if (isset($_SESSION['username'])){ echo "logout";} else {echo "Login";} ?></a>
			<!--aratam buton login-->
            <a class="navbar-brand nav navbar-nav navbar-right" href="userpage.php"> <?php if (isset($_SESSION['username'])){ echo "Contul tau:",$_SESSION['username'];} ?></a>
            <!--daca userul este logat, aratam numele in bara de sus!-->
		</div>
    </div>
</div>
<br>


<div id="mySidenav" class="sidenav">
  <!--<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>-->

  <form method="post" enctype="multipart/form-data" class="form-horizontal" style="background:rgba(5, 255, 255, 0.2);">


    <?php
	if(isset($errMSG)){
		?>
        <div class="alert alert-danger">
          <span class="glyphicon glyphicon-info-sign"></span> &nbsp; <?php echo $errMSG; ?>
        </div>
        <?php
	}
	?>


	<table class="table table-bordered table-responsive">

    <tr>
    	<td>
    		<label class="control-label">Locatie</label>
    	</td>
        <td>
 
        	<input class="form-control" type="text" name="userLocatie" maxlength="32" />
        </td>
    </tr>


    <tr>
    	<td>
    		<label class="control-label">Rasa</label>
    	</td>
        <td>
        	<select class="form-control" name="userBreed" required>
				<?php

					$stmt = $DB_con->prepare('SELECT DISTINCT(userBreed) as breed from tabel_caini ORDER by breed ASC');
					$stmt->execute();

					if($stmt->rowCount() > 0){
						while($row=$stmt->fetch(PDO::FETCH_ASSOC))
						{
							extract($row);
				?>
				        <option value="" selected disabled>--</option>
						<option 
							value="<?php echo $breed ?>" 
							<?php if (isset($_POST['userBreed']) && $_POST['userBreed'] == $breed) echo "selected='selected'";?>
						>
							<?php echo $breed ?>
						</option>
				<?php
						}		
					}		
				?>
  			</select>   
        	
        </td>
    </tr>

    <tr>
    	<td>
    		<label class="control-label">Sex</label>
    	</td>
        <td>
        	<select class="form-control" name="userSex" required>
				<?php

					$stmt = $DB_con->prepare('SELECT DISTINCT(userSex) as sex from tabel_caini');
					$stmt->execute();

					if($stmt->rowCount() > 0){
						while($row=$stmt->fetch(PDO::FETCH_ASSOC))
						{
							extract($row);
				?>
                        <option value="" selected disabled>--</option>
						<option 
							value="<?php echo $sex ?>" 
							<?php if (isset($_POST['userSex']) && $_POST['userSex'] == $sex) echo "selected='selected'";?>
						>
							<?php echo $sex ?>
						</option>						
				<?php
						}		
					}		
				?>
  			</select>        	
        </td>
    </tr>

    <tr>
    	<td>
    		<label class="control-label">Varsta</label>
    	</td>
        <td>
        	<select class="form-control" name="userAge" required>
				<?php

					$stmt = $DB_con->prepare('SELECT DISTINCT(userAge) as age from tabel_caini ORDER BY Age ASC');
					$stmt->execute();

					if($stmt->rowCount() > 0){
						while($row=$stmt->fetch(PDO::FETCH_ASSOC))
						{
							extract($row);
				?>
                        <option value="" selected disabled>--</option>
						<option 
							value="<?php echo $age ?>" 
							<?php if (isset($_POST['userAge']) && $_POST['userAge'] == $age) echo "selected='selected'";?>
						>
							<?php echo $age ?>
						</option>	
				<?php
						}		
					}		
				?>
  			</select>          	
        </td>
    </tr>

    <tr>
        <td colspan="2">
        <button type="submit" class="btn btn-default">
        	Filtreaza
        </button>
        </td>
    </tr>

    </table>

</form>

</div>


	<!-- div pentru anunturi !-->

	<div class="container" style="width: 80%;  padding-left:9%;">

		<div class="page-header">
	    	<h1  class="h2" style="text-align: center;color: white;padding-left: 12%;"> Toate anunturile de pe site <a class="btn btn-default" href="indexlogin.php" style="float: right;"> <span class="glyphicon glyphicon-plus"></span> &nbsp; Adauga un caine spre adoptie: </a></h1>
	    </div>




	    <div style="color:white;font-size: 20px;text-align: center;padding-right: 10%;">
	    	<p>Numarul de caini adoptati:
	<?php
	$con=mysqli_connect("localhost","root","","testdb");
	$result=mysqli_query($con,"SELECT COUNT(*) adoptat FROM Tabel_caini WHERE adoptat = 0");
	while ($row = $result->fetch_assoc()) {
	    echo $row['adoptat'];
	}?> 🐶
	</p>

				</div>



	<div class="row" id="anunturile">

	<?php
		
		if(isset($_POST) && (isset($_POST['userLocatie']) || isset($_POST['userBreed']) || isset($_POST['userSex']) || isset($_POST['userAge']))){

			$query = 'SELECT userID, userName, userBreed ,userSex,userAge,userLocatie, userPic, userTel,adoptat 
					  FROM Tabel_caini 
					  WHERE ';


			foreach($_POST as $key => $value){
				if($key != 'userLocatie'){
					$query = $query . " ${key} = '${value}' AND ";

				}else if(isset($_POST['userLocatie']) && $_POST['userLocatie'] != ''){
					$query = $query . " ${key} like '". strtolower($value) ."' AND ";
				}
			}
			$query = substr($query, 0, strrpos($query, 'AND')) . 'ORDER BY userID DESC LIMIT 9';
			$stmt = $DB_con->prepare($query);
		}else{
			$stmt = $DB_con->prepare('SELECT userID, userName, userBreed ,userSex,userAge,userLocatie, userPic, userTel,adoptat FROM Tabel_caini ORDER BY userID DESC LIMIT 9');
		}
		
		
		$stmt->execute();

		if($stmt->rowCount() > 0)
		{
			while($row=$stmt->fetch(PDO::FETCH_ASSOC))
			{
				extract($row);
	?>

		<style>.page-header {
	    max-width: 100%;
	}
	             .page-header {
	    word-wrap: break-word;
	}	
	    		.card:hover{
	    transition: transform 1.3s;
	    transform: scale(0.9);
	    
	}	</style>
				

		<?php if ($adoptat){?>
			<a href="dogpage.php?idAnunt=<?php echo $userID ?>">
	 	<div class="card col-xs-4" style="width:285px;border: 2px solid white;margin: 3px; background:rgba(32,32,32, 0.4);margin-bottom: 10px;">

	 	<?php } else {?>

	 		<div class="col-xs-4" style="width:285px;border: 2px solid white;margin: 3px; background-image: url('images/adoptat.png');  opacity: 0.7; margin-bottom: 10px;">


	 		<?php }?>
	 		
	 		<div class="card-body" style="padding-left: 5%;">
	 			
					<h4 class="card-title" style="font-weight:bold;color: white;"><?php echo $userName."&nbsp;/&nbsp;".$userBreed; ?></h4>
					<img src="user_images/<?php echo $row['userPic']; ?>" class="card-img-bottom img-rounded" width="220px" height="200px" style="margin-bottom: 10px;" />

					<?php if($userSex == "M") { ?>
						<p class="card-text" style="color: white;">Sex: Mascul</p>
					<?php } else {?>
						<p class="card-text" style="color: white;">Sex: Femela</p>
					<?php } ?>
					<p class="card-text" style="color: white;">Varsta:<?php echo $userAge; ?></p>
						<p class="card-text" style="color: white;">Locatie:<?php echo $userLocatie; ?></p>
					<p class="card-text" style="color: white;margin-bottom: 20px;">Numar de telefon:<?php echo $userTel; ?></p>
				
			</div>
				
				
	    </div>
	</a>
				<?php
			}
		}
		else
		{
			?>
	        <div class="col-xs-12">
	        	<div class="alert alert-warning">
	            	<span class="glyphicon glyphicon-info-sign"></span> &nbsp; Nu s-a gasit nimic ...
	            </div>
	        </div>
	        <?php
		}

	?>


</div>

	<div class="text-center" style="padding-right: 10%;margin-top: 50px;margin-bottom: 25px;"> 
	<button id="loadButton" class="btn btn-primary btn-lg">Arata mai multi caini</button>
	</div>
	</div>


<script src="bootstrap/js/bootstrap.min.js"></script> 

</body>

</html>
