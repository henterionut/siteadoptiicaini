<?php
	session_start();

	if (!isset($_SESSION['username'])) {
		$_SESSION['msg'] = "Trebuie sa te loghezi mai intai!";
		header('location: login.php');
	}

	if (isset($_GET['logout'])) {
		session_destroy();
		unset($_SESSION['username']);
		header("location: login.php");
	}
?>




<?php

	error_reporting( ~E_NOTICE ); //
	require_once 'dbconfig.php';

	if(isset($_POST['btnsave']))
	{
		$username = $_POST['user_name'];// nume caine
		$userbreed = $_POST['user_breed'];// rasa caine
		$usersex = $_POST['user_sex'];// sex caine
		$userage = $_POST['user_age'];// varsta caine
		$userlocatie = $_POST['user_locatie'];

		$imgFile = $_FILES['user_image']['name'];
		$tmp_dir = $_FILES['user_image']['tmp_name'];
		$imgSize = $_FILES['user_image']['size'];

		$usertel = $_POST['user_tel'];

//>>>>>>>>>>>>>>>
		$userid = $_SESSION['userId'];
//>>>>>>>>>>>>>>>

		if(empty($username)){
			$errMSG = "Va rog sa introduceti un nume.";
		}
		else if(empty($userbreed)){
			$errMSG = "Va rog sa introduceti rasa cainelui.";
		}
		else if(empty($imgFile)){
			$errMSG = "Va rog sa selectati o imagine cu cainele.";
		}
		else
		{
			$upload_dir = 'user_images/'; // unde vor fii uploadate pozele

			$imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // vezi extensia imaginii

			// valideaza extensia imaginii.
			$valid_extensions = array('jpeg', 'jpg', 'png', 'gif');

			// da un nume aleatoriu imaginii uploadate
			$userpic = rand(1000,1000000).".".$imgExt;

			// daca imaginea are un fomat valid
			if(in_array($imgExt, $valid_extensions)){
				// verifica dimensiunea imaginii
				if($imgSize < 5000000)				{
					move_uploaded_file($tmp_dir,$upload_dir.$userpic);
				}
				else{
					$errMSG = "Ne pare rau , poza ta este prea mare(>5MB)";
				}
			}
			else{
				$errMSG = "Aceptam doar imagini in format JPG, JPEG, PNG & GIF.";
			}
		}


		// daca nu au fost gasite errori , continua ....
		if(!isset($errMSG))
		{
			$stmt = $DB_con->prepare('INSERT INTO Tabel_caini(userName,userBreed,userSex,userAge,userLocatie,userPic,userTel,userAcc) VALUES(:uname, :ubreed, :usex,:uage,:uloc, :upic,:utel,:uacc)');
			$stmt->bindParam(':uname',$username);
			$stmt->bindParam(':ubreed',$userbreed);
			$stmt->bindParam(':usex',$usersex);
			$stmt->bindParam(':uage',$userage);
			$stmt->bindParam(':uloc',$userlocatie);
			$stmt->bindParam(':upic',$userpic);
			$stmt->bindParam(':utel',$usertel);
			$stmt->bindParam(':uacc',$userid);
			if($stmt->execute())
			{
				$successMSG = "Felicitari! Am adaugat anuntul ! ...";
				header("refresh:2;index2.php"); // te redirectioneaza dupa 2 sec
			}
			else
			{
				$errMSG = "Error....";
			}
		}
	}
?>



<!DOCTYPE html>
<html>


<head>
	<title>Licenta</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="style.css">
	
</head>



<body style="background: url(images/bg2.jpg)no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;">

	<div class="header" style="background-color: green;"">
		<h2>Adauga anunt</h2>
	</div>

	<div class="content" style="background-color: green;">
	<!-- notificatie -->
	<?php if (isset($_SESSION['success'])) : ?>
			<div class="error success" >
				<h3>
					<?php
						echo $_SESSION['success'];
						unset($_SESSION['success']);
					?>
				</h3>
			</div>

		<?php endif ?>

		<!-- arata informatia despre userul logat -->
		<?php  if (isset($_SESSION['username'])) : ?>
			<h5 style="color:white;">Salut <strong><?php echo $_SESSION['username']; ?>,completeaza formularul pentru a adauga un caine. </strong></h5>
			<p> <a href="indexlogin.php?logout='1'" style="color: red;">logout</a> </p>
		<?php endif ?>
	</div>
	

		<?php
		if(isset($errMSG)){
				?>
	            <div class="alert alert-danger">
	            	<span class="glyphicon glyphicon-info-sign"></span> <strong><?php echo $errMSG; ?></strong>
	            </div>
	            <?php
		}
		else if(isset($successMSG)){
			?>
	        <div class="alert alert-success">
	              <strong><span class="glyphicon glyphicon-info-sign"></span> <?php echo $successMSG; ?></strong>
	        </div>
	        <?php
		}
		?>

	<form method="post" enctype="multipart/form-data" class="form-horizontal" style="background:rgba(5, 255, 255, 0.2); ">

		

	    <tr>
	    	<td><label class="control-label">Nume caine:</label></td>
	        <td><input class="form-control" type="text" name="user_name" maxlength="8" placeholder="Introduceti numele cainelui" value="<?php echo $username; ?>" /></td>
	    </tr>

	    <tr>
	    	<td><label class="control-label">Rasa caine:</label></td>
	        <td><input class="form-control" type="text" name="user_breed" maxlength="15"  placeholder="Introduceti rasa cainelui" value="<?php echo $userbreed; ?>" /></td>
	    </tr>

			 <tr>

	    	<td><label class="control-label">Sex:</label></td>
	        <td>
	        	<select class="form-control"  name="user_sex" required>
	        		<option value="" selected disabled>Selecteaza sex </option>
	        		<option value="M">Mascul </option>
        			<option value="F"> Femela </option>
	        	</select>
	        </td>
	    </tr>

			<tr>
	    	<td><label class="control-label">Varsta:</label></td>
	        <td><input class="form-control" type="text" name="user_age" maxlength="15"  placeholder="Introduceti varsta cainelui" value="<?php echo $userage; ?>" /></td>
	    </tr>

	    <tr>
	    	<td><label class="control-label">Locatie:</label></td>
	        <td><input class="form-control" type="text" name="user_locatie" maxlength="20" placeholder="Judet/Oras" value="<?php echo $userlocatie; ?>" /></td>
	    </tr>

	    <tr>
	    	<td><label class="control-label">Poza:</label></td>
	        <td><input class="input-group" type="file" name="user_image" accept="image/*" /></td>
	    </tr>

	    <tr>
	    	<td><label class="control-label">Telefon:</label></td>
	        <td><input class="form-control" type="number" name="user_tel" maxlength="16" placeholder="Introduceti nr de telefon" value="<?php echo $usertel; ?>" /></td>
	    </tr>
<br>
	    <tr>
	        <td colspan="2"><button type="submit" name="btnsave" class="btn btn-default">
	        <span class="glyphicon glyphicon-save"></span> &nbsp; Trimite
	        </button>
	        </td>
	    </tr>

	    </table>

	</form>



	<h4 style="text-align: center;font-weight:bold;color: white;">Ca sa vezi toti cainii adaugati,apasa aici.</h4>
	<h4 style="text-align: center;"> <a class="btn btn-default" href="index2.php"> <span class="glyphicon glyphicon-eye-open"></span> &nbsp; Vezi toti cainii </a></h4>



	</div>

	<!-- JavaScript -->
	<script src="bootstrap/js/bootstrap.min.js"></script>



</body>
</html>
