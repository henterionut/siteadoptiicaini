<?php

  require_once 'dbconfig.php';
 
  if(isset($_GET['delete_id']))
  {
    // selecteaza img din baza de date care trb stearsa
    // $stmt_select = $DB_con->prepare('SELECT userPic FROM Tabel_caini WHERE userID =:uid');
    // $stmt_select->execute(array(':uid'=>$_GET['delete_id']));
    // $imgRow=$stmt_select->fetch(PDO::FETCH_ASSOC);
    // unlink("user_images/".$imgRow['userPic']);

    // o sa stearga un "record" din baza de date
    $stmt_delete = $DB_con->prepare('UPDATE Tabel_caini SET adoptat=0 WHERE userID =:uid');
    $stmt_delete->bindParam(':uid',$_GET['delete_id']);
    $stmt_delete->execute();

    header("Location: userpage.php");
  }

?>
<!DOCTYPE html>
<html>

<head>

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" />
  <title>Licenta</title>
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.min.css">
  <link rel="stylesheet" type="text/css" href="style.css">

</head>


<body style="background: url(images/bg1.jpg)no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;">


<div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container-fluid">

        <div class="navbar-header">
          <?php session_start();?> 
      <a class="navbar-brand" href="index.php" title='Acasa'>Acasa</a>
            <a class="navbar-brand" href="indexlogin.php">Adauga anunt</a>
           </div>
           <a class="navbar-brand nav navbar-nav navbar-right" href="indexlogin.php?logout='1'" style="color: red;"><?php if (isset($_SESSION['username'])){ echo "logout";} else {echo "Login";} ?></a>
      <!--aratam buton login-->
            <a class="navbar-brand nav navbar-nav navbar-right" href="userpage.php"> <?php if (isset($_SESSION['username'])){ echo "Contul tau:",$_SESSION['username'];} ?></a>
            <!--daca userul este logat, aratam numele in bara de sus!-->
      

         </div>

    </div>
</div>
<br>
<br>

  <div class="container" style="width: 80%;  padding-left:10%;">
    

    <h1 style="color: white;"> Salut <?php if (isset($_SESSION['username'])){ echo $_SESSION['username'];} ?>,aici sunt toate anunturile tale:</h1>
    




     <div class="row">
<?php
  require_once 'dbconfig.php';
  $stmt =$DB_con->prepare("SELECT DISTINCT userID, tabel_caini.userName, userBreed ,userSex,userAge,userLocatie, userPic,userTel ,userAcc FROM tabel_caini JOIN tabel_utilizatori
WHERE Tabel_caini.userAcc='{$_SESSION['userId']}' AND adoptat=1 ORDER BY userID DESC");
  $stmt->execute();

  if($stmt->rowCount() > 0)
  {
    while($row=$stmt->fetch(PDO::FETCH_ASSOC))
    {
      extract($row);

      ?>

    

     
  <div class="card col-xs-4" style="width:280px;border: 2px solid white;margin: 3px; background:rgba(5, 255, 255, 0.2);">  
    <div class="card-body" style="padding-bottom: 10px;padding-left: 13px;">
     
          <h4 class="card-title" style="font-weight:bold;color: white;"><?php echo $userName."&nbsp;/&nbsp;".$userBreed; ?></h4>
          <img src="user_images/<?php echo $row['userPic']; ?>" class="card-img-bottom img-rounded" width="220px" height="200px" style="margin-bottom: 20px;" />
          <p class="card-text" style="color: white;">Sex:<?php echo $userSex; ?></p>
          <p class="card-text" style="color: white;">Varsta:<?php echo $userAge; ?></p>
          <p class="card-text" style="color: white;">Locatie:<?php echo $userLocatie; ?></p>
          <p class="card-text" style="color: white;">Numar de telefon:<?php echo $userTel; ?></p>

          <a class="btn btn-info" href="editform.php?edit_id=<?php echo $row['userID']; ?>" title="click for edit" onclick="return confirm('Vrei sa editezi anuntul ?')"><span class="glyphicon glyphicon-edit"></span> Editeaza</a>
          <a class="btn btn-danger" href="?delete_id=<?php echo $row['userID']; ?>" title="click for delete" onclick="return confirm('Vrei sa stergi anuntul ?')"><span class="glyphicon glyphicon-remove-circle"></span> Sterge</a>
              
     </div>
      </div>        
       
          
 

   
  
      <?php
    }
  }
  else
  {
    ?>
        <div class="col-xs-12">
          <div class="alert alert-warning">
              <span class="glyphicon glyphicon-info-sign"></span> &nbsp; Nu s-a gasit nimic ...
            </div>
        </div>
        <?php
  }

?>
</div>





</div>
</body>
</html>



