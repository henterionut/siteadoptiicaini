<?php

	error_reporting( ~E_NOTICE );

	require_once 'dbconfig.php';

	if(isset($_GET['edit_id']) && !empty($_GET['edit_id']))
	{
		$id = $_GET['edit_id'];
		$stmt_edit = $DB_con->prepare('SELECT userName, userBreed, userSex,userAge,userLocatie, userPic,userTel FROM Tabel_caini WHERE userID =:uid');
		$stmt_edit->execute(array(':uid'=>$id));
		$edit_row = $stmt_edit->fetch(PDO::FETCH_ASSOC);
		extract($edit_row);
	}
	else
	{
		header("Location: index2.php");
	}



	if(isset($_POST['btn_save_updates']))
	{
		$username = $_POST['user_name'];
		$userbreed = $_POST['user_breed'];
		$usersex = $_POST['user_sex'];
		$userage = $_POST['user_age'];
		$userlocatie = $_POST['user_locatie'];
		$usertel = $_POST['user_tel'];

		$imgFile = $_FILES['user_image']['name'];
		$tmp_dir = $_FILES['user_image']['tmp_name'];
		$imgSize = $_FILES['user_image']['size'];

		if($imgFile)
		{
			$upload_dir = 'user_images/';
			$imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION));
			$valid_extensions = array('jpeg', 'jpg', 'png', 'gif');
			$userpic = rand(1000,1000000).".".$imgExt;
			if(in_array($imgExt, $valid_extensions))
			{
				if($imgSize < 5000000)
				{
					unlink($upload_dir.$edit_row['userPic']);
					move_uploaded_file($tmp_dir,$upload_dir.$userpic);
				}
				else
				{
					$errMSG = "Poza trebuie sa fie mai mica de 5MB.";
				}
			}
			else
			{
				$errMSG = "Aceptam numai fisiere de tip JPG, JPEG, PNG & GIF.";
			}
		}
		else
		{
			// daca nu selecteaza alta imagine, atunci ramane imaginea deja alesa
			$userpic = $edit_row['userPic']; // vechea img din baza de date
		}


		// daca nu sunt errori, continua...
		if(!isset($errMSG))
		{
			$stmt = $DB_con->prepare('UPDATE Tabel_caini
									     SET userName=:uname,
										     userBreed=:ubreed,
											 userSex=:usex,
											 userAge=:uage,
											 userLocatie=:uloc,
										     userPic=:upic,
										     userTel=:utel
								       WHERE userID=:uid');
			$stmt->bindParam(':uname',$username);
			$stmt->bindParam(':ubreed',$userbreed);
			$stmt->bindParam(':usex',$usersex);
			$stmt->bindParam(':uage',$userage);
			$stmt->bindParam(':uloc',$userlocatie);
			$stmt->bindParam(':upic',$userpic);
			$stmt->bindParam(':utel',$usertel);
			$stmt->bindParam(':uid',$id);

			if($stmt->execute()){
				?>
                <script>
				alert('Anuntul a fost updatat cu sucess ...');
				window.location.href='index2.php';
				</script>
                <?php
			}
			else{
				$errMSG = "Scuze, nu am putut updata anuntul !";
			}

		}


	}

?>
<!DOCTYPE html PUBLIC>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Editare</title>

<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">

<!-- Optional  -->
<link rel="stylesheet" href="bootstrap/css/bootstrap-theme.min.css">

<!-- customcss -->


<!--JavaScript -->
<script src="bootstrap/js/bootstrap.min.js"></script>

<script src="jquery-1.11.3-jquery.min.js"></script>
</head>
<body style="background: url(images/bg2.jpg)no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;">

<div class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">

			<div class="navbar-header">
        	<?php session_start();?> 
			<a class="navbar-brand" href="index.php" title='Acasa'>Acasa</a>
            <a class="navbar-brand" href="indexlogin.php">Adauga anunt</a>
           </div>
           <a class="navbar-brand nav navbar-nav navbar-right" href="indexlogin.php?logout='1'" style="color: red;"><?php if (isset($_SESSION['username'])){ echo "logout";} else {echo "Login";} ?></a>
			<!--aratam buton login-->
            <a class="navbar-brand nav navbar-nav navbar-right" href="userpage.php"> <?php if (isset($_SESSION['username'])){ echo "Contul tau:",$_SESSION['username'];} ?></a>
            <!--daca userul este logat, aratam numele in bara de sus!-->
			

         </div>

    </div>
</div>


<div class="container">


	<div class="page-header">
    	<h1 class="h2">Editeaza anuntul</h1>
    </div>

<div class="clearfix"></div>

<form method="post" enctype="multipart/form-data" class="form-horizontal" style="background:rgba(5, 255, 255, 0.2);">


    <?php
	if(isset($errMSG)){
		?>
        <div class="alert alert-danger">
          <span class="glyphicon glyphicon-info-sign"></span> &nbsp; <?php echo $errMSG; ?>
        </div>
        <?php
	}
	?>


	<table class="table table-bordered table-responsive">

    <tr>
    	<td><label class="control-label">Nume caine:</label></td>
        <td><input class="form-control" type="text" name="user_name" maxlength="8" value="<?php echo $userName; ?>" required /></td>
    </tr>

    <tr>
    	<td><label class="control-label">Rasa caine:</label></td>
        <td><input class="form-control" type="text" name="user_breed" maxlength="15" value="<?php echo $userBreed; ?>" required /></td>
    </tr>

	<tr>
    	<td><label class="control-label">Sex:</label></td>
        <td><input class="form-control" type="text" name="user_sex" maxlength="10" value="<?php echo $userSex; ?>" required /></td>
    </tr>
    <tr>
    	<td><label class="control-label">Varsta:</label></td>
        <td><input class="form-control" type="text" name="user_age" maxlength="20" value="<?php echo $userAge; ?>" required /></td>
    </tr>

    <tr>
    	<td><label class="control-label">Locatie:</label></td>
        <td><input class="form-control" type="text" name="user_locatie" maxlength="20" value="<?php echo $userLocatie; ?>" required /></td>
    </tr>

    <tr>
    	<td><label class="control-label">Poza caine:</label></td>
        <td>
        	<p><img src="user_images/<?php echo $userPic; ?>" height="150" width="150" /></p>
        	<input class="input-group" type="file" name="user_image" accept="image/*" />
        </td>
    </tr>
    <tr>
    	<td><label class="control-label">Nr Telefon:</label></td>
        <td><input class="form-control" type="text" name="user_tel" maxlength="12" value="<?php echo $userTel; ?>" required /></td>
    </tr>

    <tr>
        <td colspan="2"><button type="submit" name="btn_save_updates" class="btn btn-default">
        <span class="glyphicon glyphicon-save"></span> Update
        </button>

        <a class="btn btn-default" href="index2.php"> <span class="glyphicon glyphicon-backward"></span> Renunta </a>

        </td>
    </tr>

    </table>

</form>




</div>
</body>
</html>
