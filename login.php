<?php include('server.php') ?>
<!DOCTYPE html>
<html>
<head>
	<title>Login/Register</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body style="background: url(images/bg2.jpg)no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;">

	<div class="header" style="background-color: green;">
		<h2>Trebuie sa fii logat pe site ca sa adaugi un anunt!</h2>
	</div>

	<form method="post" action="login.php" style="background:rgba(5, 255, 255, 0.2);">

		<?php include('errors.php'); ?>

		<div class="input-group">
			<label>Nume de utilizator</label>
			<input type="text" name="username" >
		</div>
		<div class="input-group">
			<label>Parola</label>
			<input type="password" name="password">
		</div>
		<div class="input-group">
			<button type="submit" class="btn" name="login_user">Login</button>
		</div>
		<p>
			Nu ai cont? <a href="register.php">Inregistrare</a>
		</p>
		<a href="index.php"><-Inapoi</a>
	</form>



</body>
</html>
