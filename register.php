<?php include('server.php') ?>
<!DOCTYPE html>
<html>
<head>
	<title>Registration system PHP and MySQL</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body style="background: url(images/bg2.jpg)no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;">

	<div class="header" style="background-color: green;">
		<h2>Creare cont</h2>
	</div>
	
	<form method="post" action="register.php" style="background:rgba(5, 255, 255, 0.2);">

		<?php include('errors.php'); ?>

		<div class="input-group">
			<label>Nume de utilizator</label>
			<input type="text" name="username" value="<?php echo $username; ?>">
		</div>
		<div class="input-group">
			<label>Email</label>
			<input type="email" name="email" value="<?php echo $email; ?>">
		</div>
		<div class="input-group">
			<label>Parola</label>
			<input type="password" name="password_1">
		</div>
		<div class="input-group">
			<label>Confirma parola</label>
			<input type="password" name="password_2">
		</div>
		<div class="input-group">
			<button type="submit" class="btn" name="reg_user">Inregistrare</button>
		</div>
		<p>
			Ai deja cont? <a href="login.php">Log in</a>
		</p>
	</form>
</body>
</html>