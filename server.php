<?php
	session_start();

	// declaram variabilele
	$username = "";
	$email    = "";
	$errors = array();
	$_SESSION['success'] = "";

	// conectare
	$db = mysqli_connect('localhost', 'root', '', 'testdb');

	// REGISTER
	if (isset($_POST['reg_user'])) {
		// primim toate valorile de intrare din formular
		$username = mysqli_real_escape_string($db, $_POST['username']);
		$email = mysqli_real_escape_string($db, $_POST['email']);
		$password_1 = mysqli_real_escape_string($db, $_POST['password_1']);
		$password_2 = mysqli_real_escape_string($db, $_POST['password_2']);

		// validare formular: ne asiguram ca formularul este completat corect
		
		if (empty($username)) { array_push($errors, "Acest camp trebuie completat"); }
		if (empty($email)) { array_push($errors, "Acest camp trebuie completat"); }
// verifica daca email-ul ii valid
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) { array_push($errors, "Email invalid"); }
		if (empty($password_1)) { array_push($errors, "Acest camp trebuie completat"); }

		if ($password_1 != $password_2) {
			array_push($errors, "Parolele nu se potrivesc!!!");
		}

		// inregistram utilizatorul daca nu sunt errori
		if (count($errors) == 0) {
			$password = md5($password_1);//encriptam parola înainte de a o salva în baza de date
			$query = "INSERT INTO tabel_utilizatori (username, email, password)
					  VALUES('$username', '$email', '$password')";
			mysqli_query($db, $query);
			$query = "SELECT * FROM tabel_utilizatori WHERE username='$username' AND password='$password'";
			$results = mysqli_query($db, $query);

			if (mysqli_num_rows($results) == 1) {
				$_SESSION['username'] = $username;
			//Aici luam id-ul de la userul inregistrat pe site !!
				$row = mysqli_fetch_array($results);
				$_SESSION['userId'] = $row['id'];
				$_SESSION['success'] = "Felicitari,te-ai inregistrat cu succes !"; //mesaj dupa inregistrare
				header('location: indexlogin.php');
			}

		}

	}

	// ...

	// LOGIN
	if (isset($_POST['login_user'])) {
		$username = mysqli_real_escape_string($db, $_POST['username']);
		$password = mysqli_real_escape_string($db, $_POST['password']);

		if (empty($username)) {
			array_push($errors, "Va rugam introduceti numele de utilizator");
		}
		if (empty($password)) {
			array_push($errors, "Va rugam introduceti parola");
		}

		if (count($errors) == 0) {
			$password = md5($password);
			$query = "SELECT * FROM tabel_utilizatori WHERE username='$username' AND password='$password'";
			$results = mysqli_query($db, $query);

			if (mysqli_num_rows($results) == 1) {
				$_SESSION['username'] = $username;
//Aici luam id-ul de la userul logat pe site !!
				$row = mysqli_fetch_array($results);
				$_SESSION['userId'] = $row['id'];
//--->
				$_SESSION['success'] = "Felicitari,te-ai logat cu succes."; //Mesaj dupa logare
				header('location: indexlogin.php');
			}else {
				array_push($errors, "Nume de utilizator sau parola gresita");
			}
		}
	}

?>
