<?php
  require_once 'dbconfig.php';

?>
<!DOCTYPE html PUBLIC>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" />
  <title>Licenta</title>
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.min.css">


</head>

<body style="background: url(images/bg1.jpg)no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;">

<div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container-fluid">

        <div class="navbar-header">
          <?php session_start();?> 
      <a class="navbar-brand" href="index.php" title='Acasa'>Acasa</a>
            <a class="navbar-brand" href="indexlogin.php">Adauga anunt</a>
           </div>
           <a class="navbar-brand nav navbar-nav navbar-right" href="indexlogin.php?logout='1'" style="color: red;"><?php if (isset($_SESSION['username'])){ echo "logout";} else {echo "Login";} ?></a>
      <!--aratam buton login-->
            <a class="navbar-brand nav navbar-nav navbar-right" href="userpage.php"> <?php if (isset($_SESSION['username'])){ echo "Contul tau:",$_SESSION['username'];} ?></a>
            <!--daca userul este logat, aratam numele in bara de sus!-->
      

         </div>

 </div>

<br>

<!-- div pentru anunturi !-->


<div class="container" style="padding-left:10%; float:left;width: 40%;">





<div class="row">
<?php
  $userId = $_GET['idAnunt'];

  $stmt = $DB_con->prepare("SELECT DISTINCT userID, tabel_caini.userName, userBreed ,userSex,userAge,userLocatie, userPic,userTel ,userAcc FROM tabel_caini 
WHERE Tabel_caini.userID=".$userId);
  $stmt->execute();

  if($stmt->rowCount() > 0)
  {
    while($row=$stmt->fetch(PDO::FETCH_ASSOC))
    {
      extract($row);
      ?>

      <style>.page-header {
    max-width: 100%;

                          }
             .page-header {
    word-wrap: break-word;
    }   </style>
      
<div class="page-header">
      <h1 class="card-title" style="font-weight:bold;color: white;"><?php echo $userName."&nbsp;/&nbsp;".$userBreed; ?></h1>
    </div>


  <div class="card col-xs-4" style="width:290px;border: 2px solid white;margin: 3px; background:rgba(5, 255, 255, 0.2);">
    
    <div class="card-body">
      <div class="container">
       <br>
        <img src="user_images/<?php echo $row['userPic']; ?>" class="card-img-bottom img-rounded" width="220px" height="200px" style="margin-bottom: 20px;" />
        <p class="card-text" style="color: white;">Sex:<?php echo $userSex; ?></p>
        <p class="card-text" style="color: white;">Varsta:<?php echo $userAge; ?></p>
          <p class="card-text" style="color: white;">Locatie:<?php echo $userLocatie; ?></p>
        <p class="card-text" style="color: white;">Numar de telefon:<?php echo $userTel; ?></p>
      </div>
    </div>
      
      
    </div>

      
      <?php
    }
  }
  else
  {
    ?>
        <div class="col-xs-12">
          <div class="alert alert-warning">
              <span class="glyphicon glyphicon-info-sign"></span> &nbsp; Nu s-a gasit nimic ...
            </div>
        </div>
        <?php
  }

?>
</div>

</div> <!-- Final div ptr anunturi-->



<?php

$query = " SELECT email from tabel_Utilizatori join tabel_caini on tabel_caini.userAcc = tabel_utilizatori.id where tabel_caini.userID= $userId LIMIT 1";
$results = $DB_con->prepare( $query);
$results->execute();

$email = $results->fetch(PDO::FETCH_ASSOC)['email'];
?>

<!-- ^^^^luam email-ul din db pentru anuntul respectiv-->


<div class="container" style="float:left;width: 40%;padding-top: 8%;position: relative;">
    <h3 class="text-center" style="color:white;">Trimite un e-mail:</h3>

    <div >
        <div class="form-holder">
            <!--Form-->
            <form action="sendgrid-php/test.php" method="post">
                
                <div class="form-group">
                    <textarea rows="10" style="resize: none;" type="text" class="form-control" name="content" placeholder="Mesajul tau.."></textarea>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="from" placeholder="E-mailul tau">
                </div>
                

                <div class="form-group">
                    <input type="hidden" class="form-control" name="to" value="<?php echo $email; ?>">
                </div>
             
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Trimite</button>
                </div>
            </form>
        </div>
    </div>
</div>

</body>
<script src="bootstrap/js/bootstrap.min.js"></script>



</html>
